<?php declare(strict_types=1);

// Routing
$cfg['PmaAbsoluteUri'] = 'http://' . getenv('VIRTUAL_HOST');

// Server
$cfg['Servers'][1]['host'] = 'mysql';
$cfg['Servers'][1]['port'] = '3306';
$cfg['Servers'][1]['auth_type'] = 'config';
$cfg['Servers'][1]['user'] = 'root';
$cfg['Servers'][1]['password'] = 'root';
$cfg['Servers'][1]['hide_db'] = '^(information_schema|mysql|performance_schema)$';

// Themes
$cfg['ThemeDefault'] = 'metro';

// Page titles
$cfg['TitleDefault'] = 'PHPMyAdmin | ' . getenv('APP_NAME');
$cfg['TitleServer'] = $cfg['TitleDefault'];
$cfg['TitleDatabase'] = '@DATABASE@ | ' . getenv('APP_NAME');
$cfg['TitleTable'] = '@DATABASE@/@TABLE@ | ' . getenv('APP_NAME');
