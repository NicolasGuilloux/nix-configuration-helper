<?php

namespace App\Tests\Controller;

use RichCongress\Bundle\UnitBundle\TestCase\ControllerTestCase;
use RichCongress\Bundle\UnitBundle\TestConfiguration\Annotation\WithFixtures;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultControllerTest
 *
 * @package App\Tests\Controller
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 *
 * @covers \App\Controller\DefaultController
 * @WithFixtures
 */
class DefaultControllerTest extends ControllerTestCase
{
    /**
     * @covers \App\Controller\DefaultController::index
     *
     * @return void
     */
    public function testIndex(): void
    {
        $client = self::createClient();
        $client->request('GET', '/');

        self::assertStatusCode(Response::HTTP_OK, $client);
    }
}
