<?php declare(strict_types=1);

namespace App\Tests\Controller\Api;

use RichCongress\Bundle\UnitBundle\TestCase\ControllerTestCase;
use RichCongress\Bundle\UnitBundle\TestConfiguration\Annotation\WithContainer;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NelmioApiDocControllerTest
 *
 * @package   App\Tests\Controller\Api
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 *
 * @WithContainer
 */
class NelmioApiDocControllerTest extends ControllerTestCase
{
    /**
     * @return void
     */
    public function testNelmioApiDocSuccess(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/doc/');

        self::assertStatusCode(Response::HTTP_OK, $client);
    }
}
