<?php declare(strict_types=1);

namespace App\Tests\Controller\Api;

use RichCongress\Bundle\UnitBundle\TestCase\ControllerTestCase;
use RichCongress\Bundle\UnitBundle\TestConfiguration\Annotation\WithFixtures;
use RichCongress\Bundle\UnitBundle\TestTrait\Assertion\Parameter;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SearchControllerTest
 *
 * @package   App\Tests\Controller\Api
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 *
 * @covers \App\Controller\Api\SearchController
 * @WithFixtures
 */
class SearchControllerTest extends ControllerTestCase
{
    /**
     * @covers \App\Controller\Api\SearchController::getSearchAction()
     *
     * @return void
     */
    public function testGetSearchActionNoQuery(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/channels/nixos-20.03/search');

        self::assertStatusCode(Response::HTTP_BAD_REQUEST, $client);
    }

    /**
     * @covers \App\Controller\Api\SearchController::getSearchAction()
     *
     * @return void
     */
    public function testGetSearchAction(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/channels/nixos-20.03/search?query=test');

        self::assertStatusCode(Response::HTTP_OK, $client);
        self::assertEquals('Not implemented yet', self::getJsonContent($client));
    }

    /**
     * @covers \App\Controller\Api\SearchController::getSearchOptionsAction()
     *
     * @return void
     */
    public function testGetOptionsSearchActionNoQuery(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/channels/nixos-20.03/options/search');

        self::assertStatusCode(Response::HTTP_BAD_REQUEST, $client);
    }

    /**
     * @covers \App\Controller\Api\SearchController::getSearchOptionsAction()
     *
     * @return void
     */
    public function testGetOptionsSearchAction(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/channels/nixos-20.03/options/search?query=test');
        $content = self::getJsonContent($client);

        self::assertStatusCode(Response::HTTP_OK, $client);
        self::assertMatch(
            [
                'page'       => Parameter::integer(),
                'count'      => Parameter::integer(),
                'totalPages' => Parameter::integer(),
                'totalCount' => Parameter::integer(),
                'elements'   => Parameter::array(),
            ],
            $content
        );

        $elements = $content['elements'];

        self::assertCount(1, $elements);
        self::assertMatch(
            [
                'id'                => Parameter::integer(),
                'keyname'           => Parameter::integer(),
                'type'              => Parameter::integer(),
                'readOnly'          => Parameter::integer(),
                'defaultValue'      => Parameter::string(),
                'description'       => Parameter::string(),
                'declarations'      => Parameter::array(),
                'configurationType' => Parameter::string(),
            ],
            $elements[0]
        );
    }

    /**
     * @covers \App\Controller\Api\SearchController::getSearchPackagesAction()
     *
     * @return void
     */
    public function testGetPackagesSearchActionNoQuery(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/channels/nixos-20.03/packages/search');

        self::assertStatusCode(Response::HTTP_BAD_REQUEST, $client);
    }

    /**
     * @covers \App\Controller\Api\SearchController::getSearchPackagesAction()
     *
     * @return void
     */
    public function testGetPackagesSearchAction(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/channels/nixos-20.03/packages/search?query=number_1');
        $content = self::getJsonContent($client);

        self::assertStatusCode(Response::HTTP_OK, $client);
        self::assertMatch(
            [
                'page'       => Parameter::integer(),
                'count'      => Parameter::integer(),
                'totalPages' => Parameter::integer(),
                'totalCount' => Parameter::integer(),
                'elements'   => Parameter::array(),
            ],
            $content
        );

        $elements = $content['elements'];

        self::assertCount(1, $elements);
        self::assertMatch(
            [
                'id'                => Parameter::integer(),
                'channel'           => Parameter::string(),
                'keyname'           => Parameter::string(),
                'name'              => Parameter::string(),
                'pname'             => Parameter::string(),
                'version'           => Parameter::string(),
                'system'            => Parameter::string(),
                'available'         => Parameter::boolean(),
                'description'       => Parameter::string(),
                'downloadPage'      => Parameter::string(),
                'homepage'          => Parameter::string(),
                'license'           => Parameter::array(),
                'maintainers'       => Parameter::array(),
                'outputsToInstall'  => Parameter::array(),
                'platforms'         => Parameter::array(),
                'position'          => Parameter::string(),
                'configurationType' => Parameter::string(),
            ],
            $elements[0]
        );
    }
}
