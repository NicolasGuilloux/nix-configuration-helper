<?php

namespace App\Tests\Command;

use App\Command\AppImportOptions;
use App\Entity\Option;
use App\Repository\OptionRepository;
use RichCongress\Bundle\UnitBundle\TestCase\CommandTestCase;
use RichCongress\Bundle\UnitBundle\TestConfiguration\Annotation\WithFixtures;

/**
 * Class AppImportOptionsTest
 *
 * @package App\Tests\Command
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 *
 * @covers \App\Command\AppImportOptions
 * @covers \App\Command\AbstractImportCommand
 * @WithFixtures()
 */
class AppImportOptionsTest extends CommandTestCase
{
    /**
     * @var OptionRepository
     */
    protected $optionRepository;

    /**
     * @return void
     */
    public function beforeTest(): void
    {
        $this->command = $this->getContainer()->get(AppImportOptions::class);
        $this->optionRepository = $this->getContainer()->get(OptionRepository::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $output = $this->execute();

        self::assertStringContainsString('Importing options...', $output);
        self::assertStringNotContainsString('error', $output);

        $option = $this->optionRepository->findOneByKeyname('appstream.enable');
        self::assertInstanceOf(Option::class, $option);
        self::assertSame(1, $option->id);
        self::assertSame('appstream.enable', $option->keyname);
        self::assertSame('boolean', $option->type);
        self::assertFalse($option->readOnly);
        self::assertSame('true', $option->defaultValue);
        self::assertStringContainsString('AppStream metadata specification', $option->description);
        self::assertSame(['nixos/modules/config/appstream.nix'], $option->declarations);
    }
}