<?php

namespace App\Tests\Command;

use App\Command\AppImportPackages;
use App\Entity\Package;
use App\Repository\PackageRepository;
use DAMA\DoctrineTestBundle\Doctrine\DBAL\StaticDriver;
use RichCongress\Bundle\UnitBundle\TestCase\CommandTestCase;
use RichCongress\Bundle\UnitBundle\TestConfiguration\Annotation\WithFixtures;

/**
 * Class AppImportPackagesTest
 *
 * @package App\Tests\Command
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 *
 * @covers \App\Command\AbstractImportCommand
 * @covers \App\Command\AppImportPackages
 * @WithFixtures()
 */
class AppImportPackagesTest extends CommandTestCase
{
    /**
     * @var PackageRepository
     */
    protected $packageRepository;

    /**
     * @return void
     */
    public function beforeTest(): void
    {
        $this->command = $this->getContainer()->get(AppImportPackages::class);
        $this->packageRepository = $this->getContainer()->get(PackageRepository::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $output = $this->execute();

        StaticDriver::commit();

        self::assertStringContainsString('Importing packages for channel nixos-20.03', $output);
        self::assertStringContainsString('Importing packages for channel nixos-unstable', $output);
        self::assertStringContainsString('Importing packages for channel nixpkgs-unstable', $output);
        self::assertStringNotContainsString('error', $output);

        $package = $this->packageRepository->findOneByKeyname('_1password');
        self::assertInstanceOf(Package::class, $package);
    }
}
