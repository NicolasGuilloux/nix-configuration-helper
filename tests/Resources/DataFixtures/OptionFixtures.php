<?php declare(strict_types=1);

namespace App\Tests\Resources\DataFixtures;

use App\Entity\Option;
use RichCongress\Bundle\UnitBundle\DataFixture\AbstractFixture;

/**
 * Class OptionFixtures
 *
 * @package   App\Tests\Resources\DataFixtures
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class OptionFixtures extends AbstractFixture
{
    /**
     * @inheritDoc
     */
    protected function loadFixtures(): void
    {
        for ($i = 1; $i <= 20; $i++) {
            $this->createFromDefault('option-' . $i);
        }
    }

    public function generateDefaultEntity()
    {
        return self::buildObject(
            Option::class,
            [
                'keyname'      => 'test.option.number_' . $this->count,
                'type'         => self::pickValue(Option::TYPES),
                'readOnly'     => self::pickValue([true, false]),
                'defaultValue' => null,
                'description'  => 'Option Description n°' . $this->count,
                'declarations' => [
                    'test',
                    'option',
                    'number_' . $this->count
                ]
            ]
        );
    }

    /**
     * @param array $array
     *
     * @return mixed
     */
    protected static function pickValue(array $array)
    {
        if (count($array) === 0) {
            return null;
        }

        $clone = $array;
        shuffle($clone);

        return array_pop($clone);
    }
}
