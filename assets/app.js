// Elm
import { Elm } from './elm/Index/Main.elm';
import * as ElmDebugger from 'elm-debug-transformer';

ElmDebugger.register();

Elm.Index.Main.init({
    node: document.querySelector('main'),
    flags: {},
});

// SCSS
import './scss/app.scss';
