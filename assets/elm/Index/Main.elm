module Index.Main exposing (..)

import Browser
import Index.Data exposing (..)
import Index.Update as Update exposing (Model, Msg, initState)
import Index.View as View


main : Program Flags Model Msg
main =
    Browser.element
        { init = initState
        , view = View.view
        , update = Update.update
        , subscriptions = \_ -> Sub.none
        }
