module Index.View exposing (..)

import FontAwesome
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onCheck, onInput)
import Index.Data exposing (..)
import Index.Update exposing (Model, Msg(..))
import List


view : Model -> Html Msg
view model =
    div [ id "body", class "ui container" ]
        [ header []
            [ img [ src "/images/nix-config-helper.svg", alt "" ] []
            , h1 [] [ text "Nix Configuration Helper" ]
            ]
        , formView model
        , resultsView model
        , footer []
            [ text "Made by "
            , a [ href "https://nicolasguilloux.eu", target "_blank" ] [ text "Nicolas Guilloux" ]
            , text ". MIT License. "
            , a [ href "https://gitlab.com/NicolasGuilloux/nix-configuration-helper", target "_blank" ] [ text "Source code" ]
            ]
        ]


formView : Model -> Html Msg
formView model =
    article []
        [ header [ class "form" ]
            [ div [ class "ui search" ]
                [ input [ id "search", type_ "text", class "prompt", placeholder "Search an option or a package", value model.search, onInput MsgSearchChange ] []
                ]
            , div [ class "field checkboxes" ]
                [ div [ class "ui checkbox" ]
                    [ input [ id "options_enabled", type_ "checkbox", class "hidden", checked model.withOptions, onCheck MsgWithOptionsChange ] []
                    , label [ for "options_enabled" ]
                        [ text "Options"
                        ]
                    ]
                , div [ class "ui checkbox" ]
                    [ input [ id "packages_enabled", type_ "checkbox", class "hidden", checked model.withPackages, onCheck MsgWithPackagesChange ] []
                    , label [ for "packages_enabled" ]
                        [ text "Packages"
                        ]
                    ]
                ]
            ]
        ]


resultsView : Model -> Html Msg
resultsView model =
    div [ id "results", class "ui two column stackable grid centered" ] <|
        List.map
            (\element ->
                div [ class "column" ]
                    [ div [ class "ui raised segment" ] <|
                        case element of
                            NixOption option ->
                                optionView option

                            NixPackage package ->
                                packageView package
                    ]
            )
            model.elements


optionView : Option -> List (Html Msg)
optionView option =
    [ h3 []
        [ FontAwesome.icon FontAwesome.cogs
        , text option.keyname
        ]
    , p [] [ text option.description ]
    ]


packageView : Package -> List (Html Msg)
packageView package =
    [ h3 []
        [ FontAwesome.icon FontAwesome.boxOpen
        , text package.keyname
        ]
    , p [] [ text package.description ]
    ]
