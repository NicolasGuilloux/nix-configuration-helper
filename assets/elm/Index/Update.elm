module Index.Update exposing (..)

import Debouncer.Messages as Debouncer exposing (Debouncer, fromSeconds, toDebouncer)
import Http
import Index.Api as Api
import Index.Data exposing (..)
import Platform.Cmd
import Task


type alias Model =
    { withOptions : Bool
    , withPackages : Bool
    , search : String
    , searchDebouncer : Debouncer Msg
    , elements : List NixConfig
    }


type Msg
    = MsgGetSearchResult (Result Http.Error SearchResult)
    | MsgSearchDebouncer (Debouncer.Msg Msg)
    | MsgGetSearch
    | MsgWithOptionsChange Bool
    | MsgWithPackagesChange Bool
    | MsgSearchChange String


initState : Flags -> ( Model, Cmd msg )
initState _ =
    ( { withOptions = True
      , withPackages = False
      , search = ""
      , searchDebouncer =
            Debouncer.debounce (fromSeconds 0.75)
                |> toDebouncer
      , elements = []
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MsgGetSearchResult result ->
            case result of
                Ok searchResult ->
                    ( { model | elements = searchResult.elements }
                    , Cmd.none
                    )

                Err _ ->
                    ( model, Cmd.none )

        MsgSearchDebouncer debouncerMsg ->
            Debouncer.update update updateDebouncer debouncerMsg model

        MsgGetSearch ->
            model
                |> getSearch

        MsgWithOptionsChange bool ->
            { model | withOptions = bool, elements = [] }
                |> getSearch

        MsgWithPackagesChange bool ->
            { model | withPackages = bool, elements = [] }
                |> getSearch

        MsgSearchChange search ->
            ( { model | search = search, elements = [] }
            , sendTask (MsgSearchDebouncer (Debouncer.provideInput MsgGetSearch))
            )


getSearch : Model -> ( Model, Cmd Msg )
getSearch model =
    ( model
    , Api.getSearch model.withOptions model.withPackages model.search MsgGetSearchResult
    )


updateDebouncer : Debouncer.UpdateConfig Msg Model
updateDebouncer =
    { mapMsg = MsgSearchDebouncer
    , getDebouncer = .searchDebouncer
    , setDebouncer = \debouncer model -> { model | searchDebouncer = debouncer }
    }


sendTask : msg -> Cmd msg
sendTask msg =
    Task.succeed msg
        |> Task.perform identity
