module Index.Data exposing (..)


type alias Flags =
    {}


type alias SearchResult =
    { page : Int
    , count : Int
    , totalPages : Int
    , totalCount : Int
    , elements : List NixConfig
    }


type NixConfig
    = NixOption Option
    | NixPackage Package


type alias Option =
    { id : Int
    , keyname : String
    , type_ : String
    , readOnly : Bool
    , defaultValue : String
    , description : String
    , declarations : List String
    }


type alias Package =
    { id : Int
    , channel : String
    , keyname : String
    , name : String
    , pname : String
    , version : String
    , system : String
    , available : Bool
    , description : String
    , downloadPage : String
    , homepage : String
    , license : License
    , maintainers : List Maintainer
    , outputsToInstall : List String
    , platforms : List String
    , position : String
    }


type alias License =
    { free : Bool
    , fullName : String
    , shortName : String
    , url : Maybe String
    }


type alias Maintainer =
    { email : String
    , name : String
    }
