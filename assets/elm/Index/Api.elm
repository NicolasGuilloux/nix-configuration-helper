module Index.Api exposing (..)

import Http
import Index.Data exposing (..)
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (required)


getSearch : Bool -> Bool -> String -> (Result Http.Error SearchResult -> msg) -> Cmd msg
getSearch withOptions withPackages query msg =
    let
        getSearchAction =
            \url ->
                Http.get
                    { url = url ++ "?query=" ++ query
                    , expect = Http.expectJson msg searchResultDecoder
                    }
    in
    case ( withOptions, withPackages, query ) of
        ( _, _, "" ) ->
            Cmd.none

        ( True, True, _ ) ->
            getSearchAction "/api/channels/nixos-20.03/search"

        ( True, False, _ ) ->
            getSearchAction "/api/channels/nixos-20.03/options/search"

        ( False, True, _ ) ->
            getSearchAction "/api/channels/nixos-20.03/packages/search"

        _ ->
            Cmd.none


searchResultDecoder : Decoder SearchResult
searchResultDecoder =
    succeed SearchResult
        |> required "page" int
        |> required "count" int
        |> required "totalPages" int
        |> required "totalCount" int
        |> required "elements" (list <| nixConfigDecoder)


nixConfigDecoder : Decoder NixConfig
nixConfigDecoder =
    field "configurationType" string
        |> andThen nixConfigTypeDecoder


nixConfigTypeDecoder : String -> Decoder NixConfig
nixConfigTypeDecoder type_ =
    case type_ of
        "option" ->
            map NixOption optionDecoder

        "package" ->
            map NixPackage packageDecoder

        _ ->
            fail type_


optionDecoder : Decoder Option
optionDecoder =
    succeed Option
        |> required "id" int
        |> required "keyname" string
        |> required "type" string
        |> required "readOnly" bool
        |> required "defaultValue" string
        |> required "description" string
        |> required "declarations" (list string)


packageDecoder : Decoder Package
packageDecoder =
    succeed Package
        |> required "id" int
        |> required "channel" string
        |> required "keyname" string
        |> required "name" string
        |> required "pname" string
        |> required "version" string
        |> required "system" string
        |> required "available" bool
        |> required "description" string
        |> required "downloadPage" string
        |> required "homepage" string
        |> required "license" licenseDecoder
        |> required "maintainers" (list maintainerDecoder)
        |> required "outputsToInstall" (list string)
        |> required "platforms" (list string)
        |> required "position" string


licenseDecoder : Decoder License
licenseDecoder =
    succeed License
        |> required "free" bool
        |> required "fullName" string
        |> required "shortName" string
        |> required "url" (nullable string)


maintainerDecoder : Decoder Maintainer
maintainerDecoder =
    succeed Maintainer
        |> required "email" string
        |> required "name" string
