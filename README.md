# Nix Configuration Helper

The Nix Configuration Helper is a website that merge the Nix options with the available packages and its options aswell.

The goal is to avoid switching tabs or look into the sources to actually find an option, a package or a package configuration.

# Installation

To install it, you need to have a functioning Docker setup with docker-compsoe on a Unix machine (Linux or MacOS for instance). Moreover, you will need to free the 80 port for the nginx proxy.

- Create the proxy: 
```
docker network create proxy
```

- Create the `nginx-proxy` container:
```
docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro --name nginx-proxy --net proxy --privileged --userns=host richcongress/nginx-proxy
```

- Use `docker-compose` to start the project:
```
docker-compose up -d
```

- Install the dependencies:
```
docker-compose exec application composer install
```