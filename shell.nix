{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.docker-compose
  ];
}

stdenv.mkDerivation {
  name = "nix_helper";
  shellHook = ''
    alias dc="docker-compose"
    alias dup="dc up -d"
    alias ddown="dc down --remove-orphans"
    alias dr="ddown; dup"
    alias dcea="dc exec application"
    alias console="dcea bin/console"
    alias yarn="dcea yarn"
  '';
}
