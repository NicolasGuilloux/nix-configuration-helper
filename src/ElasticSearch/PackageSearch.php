<?php declare(strict_types=1);

namespace App\ElasticSearch;

use App\ElasticSearch\Query\BoolQuery;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;

/**
 * Class PackageSearch
 *
 * @package   App\ElasticSearch
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class PackageSearch extends AbstractSearchQuery
{
    public const FIELDS = ['keyname', 'name', 'description'];

    /**
     * @var PaginatedFinderInterface
     */
    protected $finder;

    /**
     * PackageSearch constructor.
     *
     * @param PaginatedFinderInterface $packageFinder
     */
    public function __construct(PaginatedFinderInterface $packageFinder)
    {
        $this->finder = $packageFinder;
    }

    /**
     * @param string $channel
     * @param string $query
     *
     * @return PaginatorAdapterInterface
     */
    public function searchByChannel(string $channel, string $query): PaginatorAdapterInterface
    {
        $boolQuery = static::getSearchByChannelQuery($channel, $query);

        return $this->finder->createPaginatorAdapter($boolQuery);
    }
}
