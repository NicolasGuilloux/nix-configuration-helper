<?php declare(strict_types=1);

namespace App\ElasticSearch\Index;

use Elastica\Exception\InvalidException;
use Elastica\Index;
use Elastica\Query;
use Elastica\ResultSet\BuilderInterface;
use Elastica\Search;

/**
 * Class MultiIndex
 *
 * @package   App\Search\Elastica
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class MultiIndex extends Index
{
    /**
     * Array of indices.
     *
     * @var array
     */
    protected $_indices = [];

    /**
     * Adds a index to the list.
     *
     * @param Index|string $index Index object or string
     *
     * @throws InvalidException
     *
     * @return self
     */
    public function addIndex($index): self
    {
        if ($index instanceof Index) {
            $index = $index->getName();
        }

        if (!is_scalar($index)) {
            throw new InvalidException('Invalid param type');
        }

        $this->_indices[] = (string) $index;

        return $this;
    }

    /**
     * Add array of indices at once.
     *
     * @param array $indices
     *
     * @return self
     */
    public function addIndices(array $indices = []): self
    {
        foreach ($indices as $index) {
            $this->addIndex($index);
        }

        return $this;
    }

    /**
     * Return array of indices.
     *
     * @return array List of index names
     */
    public function getIndices(): array
    {
        return $this->_indices;
    }

    /**
     * @param string|array|Query $query
     * @param int|array          $options
     * @param BuilderInterface   $builder
     *
     * @return Search
     */
    public function createSearch(
        $query = '',
        $options = null,
        BuilderInterface $builder = null
    ): Search
    {
        $search = new Search($this->getClient(), $builder);

        $search->addIndices($this->getIndices());
        $search->setOptionsAndQuery($options, $query);

        return $search;
    }

    /**
     * @param Index|array|Index[] $subIndex
     *
     * @return static
     */
    public static function createForIndex($subIndex): self
    {
        $subIndex = (array) $subIndex;
        $searchable = null;

        foreach ($subIndex as $index) {
            if ($searchable === null) {
                $searchable = new static(
                    $index->getClient(),
                    $index->getName()
                );

                $searchable->addIndex($index);

                continue;
            }

            $searchable->addIndex($index);
        }

        if ($searchable === null) {
            throw new InvalidException('No index given');
        }

        return $searchable;
    }
}
