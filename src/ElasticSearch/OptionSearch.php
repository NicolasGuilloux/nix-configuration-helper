<?php declare(strict_types=1);

namespace App\ElasticSearch;

use App\ElasticSearch\Query\BoolQuery;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;

/**
 * Class OptionSearch
 *
 * @package   App\ElasticSearch
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class OptionSearch extends AbstractSearchQuery
{
    public const FIELDS = ['keyname', 'description'];

    /**
     * @var PaginatedFinderInterface
     */
    protected $finder;

    /**
     * OptionSearch constructor.
     *
     * @param PaginatedFinderInterface $optionFinder
     */
    public function __construct(PaginatedFinderInterface $optionFinder)
    {
        $this->finder = $optionFinder;
    }

    /**
     * @param string $channel
     * @param string $query
     *
     * @return PaginatorAdapterInterface
     */
    public function searchByChannel(string $channel, string $query): PaginatorAdapterInterface
    {
        $boolQuery = static::getSearchByChannelQuery($channel, $query);

        return $this->finder->createPaginatorAdapter($boolQuery);
    }
}
