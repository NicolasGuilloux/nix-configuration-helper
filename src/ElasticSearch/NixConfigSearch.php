<?php declare(strict_types=1);

namespace App\ElasticSearch;

use App\ElasticSearch\Index\MultiIndex;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use FOS\ElasticaBundle\Index\IndexManager;
use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;
use FOS\ElasticaBundle\Paginator\TransformedPaginatorAdapter;
use FOS\ElasticaBundle\Transformer\ElasticaToModelTransformerInterface;

/**
 * Class NixConfigSearch
 *
 * @package   App\ElasticSearch
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class NixConfigSearch extends AbstractSearchQuery
{
    public const INDEX_NAMES = ['nix_config_options', 'nix_config_packages'];

    /**
     * @var ElasticaToModelTransformerInterface
     */
    protected $elasticaToModelTransformer;

    /**
     * @var IndexManager
     */
    protected $indexManager;

    /**
     * NixConfigSearch constructor.
     *
     * @param ElasticaToModelTransformerInterface $elasticaToModelTransformer
     * @param IndexManager                        $indexManager
     */
    public function __construct(
        ElasticaToModelTransformerInterface $elasticaToModelTransformer,
        IndexManager $indexManager
    ) {
        $this->elasticaToModelTransformer = $elasticaToModelTransformer;
        $this->indexManager = $indexManager;
    }

    /**
     * @param string $channel
     * @param string $query
     *
     * @return PaginatorAdapterInterface
     */
    public function searchByChannel(string $channel, string $query): PaginatorAdapterInterface
    {
        $mainQuery = new BoolQuery();
        $mainQuery->addFilter(OptionSearch::getSearchByChannelQuery($channel, $query));
        $mainQuery->addFilter(PackageSearch::getSearchByChannelQuery($channel, $query));

        $mainQuery = OptionSearch::getSearchByChannelQuery($channel, $query);

        return new TransformedPaginatorAdapter(
            $this->getMultiIndex(),
            Query::create($mainQuery),
            [],
            $this->elasticaToModelTransformer
        );
    }

    /**
     * @return MultiIndex
     */
    protected function getMultiIndex(): MultiIndex
    {
        $index = array_map(
            function (string $indexName) {
                return $this->indexManager->getIndex($indexName);
            },
            static::INDEX_NAMES
        );

        return MultiIndex::createForIndex($index);
    }
}
