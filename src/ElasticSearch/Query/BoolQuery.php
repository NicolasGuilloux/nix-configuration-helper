<?php declare(strict_types=1);

namespace App\ElasticSearch\Query;

use Elastica\Query\Match;
use Elastica\Query\MultiMatch;
use Elastica\Query\Term;

/**
 * Class BoolQuery
 *
 * @package   App\ElasticSearch\Query
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class BoolQuery extends \Elastica\Query\BoolQuery
{
    /**
     * @param string $channel
     *
     * @return void
     */
    public function addChannelFilter(string $channel): void
    {
        $term = new Term();
        $term->setTerm('channel', $channel);

        $this->addFilter($term);
    }

    /**
     * @param string $field
     * @param string $query
     *
     * @return void
     */
    public function match(string $field, string $query): void
    {
        $matchQuery = new Match();
        $matchQuery->setFieldQuery($field, $query);

        $this->addMust($matchQuery);
    }

    /**
     * @param array|string[] $fields
     * @param string $query
     *
     * @return void
     */
    public function multiMatch(array $fields, string $query): void
    {
        $multiMatchQuery = new MultiMatch();
        $multiMatchQuery->setFields($fields);
        $multiMatchQuery->setQuery($query);

        $this->addMust($multiMatchQuery);
    }
}
