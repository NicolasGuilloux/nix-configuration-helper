<?php declare(strict_types=1);

namespace App\ElasticSearch;

use App\ElasticSearch\Query\BoolQuery;
use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;

/**
 * Class AbstractSearchQuery
 *
 * @package   App\ElasticSearch
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
abstract class AbstractSearchQuery
{
    public const FIELDS = [];

    /**
     * @param string $channel
     * @param string $query
     *
     * @return PaginatorAdapterInterface
     */
    abstract public function searchByChannel(string $channel, string $query): PaginatorAdapterInterface;

    /**
     * @param string $channel
     * @param string $query
     *
     * @return BoolQuery
     */
    public static function getSearchByChannelQuery(string $channel, string $query): BoolQuery
    {
        $boolQuery = new BoolQuery();
        $boolQuery->addChannelFilter($channel);

        if (!empty($query)) {
            $boolQuery->multiMatch(static::FIELDS, $query);
        }

        return $boolQuery;
    }
}
