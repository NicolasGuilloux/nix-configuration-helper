<?php declare(strict_types=1);

namespace App\Controller\Api;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait FOSRestControllerTrait
 *
 * @package   App\Controller\Api
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
trait FOSRestControllerTrait
{
    /**
     * @param mixed|null   $object
     * @param array|string $serializationGroups
     * @param integer|null $statusCode
     *
     * @return Response
     */
    public function response($object = null, $serializationGroups = [], ?int $statusCode = null): Response
    {
        $view = new View($object, $statusCode);

        if (count((array) $serializationGroups) > 0) {
            $context = new Context();
            $context->setGroups((array) $serializationGroups);

            $view->setContext($context);
        }

        return $this->handleView($view);
    }
}
