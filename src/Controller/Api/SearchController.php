<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Dto\Out\MixedPaginatedResult;
use App\Dto\Out\OptionPaginatedResult;
use App\Dto\Out\PackagePaginatedResult;
use App\ElasticSearch\NixConfigSearch;
use App\ElasticSearch\OptionSearch;
use App\ElasticSearch\PackageSearch;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SearchController
 *
 * @package   App\Controller\Api
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class SearchController extends AbstractFOSRestController
{
    use FOSRestControllerTrait;

    /**
     * @Rest\Get("/channels/{channel}/search")
     * @Rest\QueryParam(name="query", default="", description="Search query")
     * @Rest\QueryParam(name="page", default="1", description="Page of the search")
     * @Rest\QueryParam(name="size", default="10", description="Number of items returned by the search")
     *
     * @SWG\Get(
     *     summary="Search in general",
     *     tags={"Search"},
     *     @SWG\Parameter(
     *         name="channel",
     *         in="path",
     *         type="string",
     *         description="Nix channel",
     *         default="nixos-20.03"
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Result of the search",
     *     @SWG\Schema(
     *         type="object",
     *         ref=@Nelmio\Model(type=App\Dto\Out\MixedPaginatedResult::class, groups={"search_result"})
     *     )
     * )
     *
     * @param NixConfigSearch $nixConfigSearch
     * @param string          $channel
     * @param string          $query
     * @param int             $page
     * @param int             $size
     *
     * @return Response
     */
    public function getSearchAction(
        NixConfigSearch $nixConfigSearch,
        string $channel,
        string $query,
        int $page,
        int $size
    ): Response
    {
        $paginator = $nixConfigSearch->searchByChannel($channel, $query);

        return $this->response(
            MixedPaginatedResult::createFromPaginator($page, $size, $paginator),
            'search_result'
        );
    }

    /**
     * @Rest\Get("/channels/{channel}/options/search")
     * @Rest\QueryParam(name="query", default="", description="Search query")
     * @Rest\QueryParam(name="page", default="1", description="Page of the search")
     * @Rest\QueryParam(name="size", default="10", description="Number of items returned by the search")
     *
     * @SWG\Get(
     *     summary="Search options",
     *     tags={"Search"},
     *     @SWG\Parameter(
     *         name="channel",
     *         in="path",
     *         type="string",
     *         description="Nix channel",
     *         default="nixos-20.03"
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Result of the search",
     *     @SWG\Schema(
     *         type="object",
     *         ref=@Nelmio\Model(type=App\Dto\Out\OptionPaginatedResult::class, groups={"search_result"})
     *     )
     * )
     *
     * @param OptionSearch     $optionSearch
     * @param string           $channel
     * @param string           $query
     * @param int              $page
     * @param int              $size
     *
     * @return Response
     */
    public function getSearchOptionsAction(
        OptionSearch $optionSearch,
        string $channel,
        string $query,
        int $page,
        int $size
    ): Response
    {
        $paginator = $optionSearch->searchByChannel($channel, $query);

        return $this->response(
            OptionPaginatedResult::createFromPaginator($page, $size, $paginator),
            'search_result'
        );
    }

    /**
     * @Rest\Get("/channels/{channel}/packages/search")
     * @Rest\QueryParam(name="query", default="", description="Search query")
     * @Rest\QueryParam(name="page", default="1", description="Page of the search", default="1")
     * @Rest\QueryParam(name="size", default="10", description="Number of items returned by the search")
     *
     * @SWG\Get(
     *     summary="Search packages",
     *     tags={"Search"},
     *     @SWG\Parameter(
     *         name="channel",
     *         in="path",
     *         type="string",
     *         description="Nix channel",
     *         default="nixos-20.03"
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Result of the search",
     *     @SWG\Schema(
     *         type="object",
     *         ref=@Nelmio\Model(type=App\Dto\Out\PackagePaginatedResult::class, groups={"search_result"})
     *     )
     * )
     *
     * @param PackageSearch $packageSearch
     * @param string        $channel
     * @param string        $query
     * @param int           $page
     * @param int           $size
     *
     * @return Response
     */
    public function getSearchPackagesAction(
        PackageSearch $packageSearch,
        string $channel,
        string $query,
        int $page,
        int $size
    ): Response
    {
        $paginator = $packageSearch->searchByChannel($channel, $query);

        return $this->response(
            PackagePaginatedResult::createFromPaginator($page, $size, $paginator),
            'search_result'
        );
    }
}
