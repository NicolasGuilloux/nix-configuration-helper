<?php declare(strict_types=1);

namespace App\Dto\Out;

use Doctrine\ORM\Tools\Pagination\Paginator;
use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

class PackagePaginatedResult
{
    /**
     * @var int
     *
     * @Groups({"search_result", "result_page"})
     * @SWG\Property(description="Page number of the actual search")
     */
    public $page;

    /**
     * @var int
     *
     * @Groups({"search_result", "result_count"})
     * @SWG\Property(description="Number of elements for this specific page")
     */
    public $count;

    /**
     * @var int
     *
     * @Groups({"search_result", "result_total_pages"})
     * @SWG\Property(description="Total number of pages for this search")
     */
    public $totalPages;

    /**
     * @var int
     *
     * @Groups({"search_result", "result_total_count"})
     * @SWG\Property(description="Total number of elements for this search")
     */
    public $totalCount;

    /**
     * @var Paginator
     *
     * @Groups({"search_result", "result_elements"})
     * @SWG\Property(
     *     type="array",
     *     description="List of the packages for this search",
     *     @SWG\Items(
     *         ref=@Nelmio\Model(type=App\Entity\Package::class)
     *     ),
     * )
     */
    public $elements;

    /**
     * @param int                       $page
     * @param int                       $size
     * @param PaginatorAdapterInterface $paginator
     *
     * @return $this
     */
    public static function createFromPaginator(int $page, int $size, PaginatorAdapterInterface $paginator): self
    {
        $offset  = ($page - 1) * $size;

        $dto = new static();
        $dto->page = $page;
        $dto->elements = $paginator->getResults($offset, $size)->toArray();
        $dto->count = count($dto->elements);
        $dto->totalCount = $paginator->getTotalHits();
        $dto->totalPages = intdiv($dto->totalCount - 1, $size) + 1;

        return $dto;
    }
}
