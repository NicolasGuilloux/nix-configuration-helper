<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200426134040 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `option` (id INT AUTO_INCREMENT NOT NULL, channel VARCHAR(30) NOT NULL, keyname VARCHAR(255) NOT NULL, type LONGTEXT NOT NULL, read_only TINYINT(1) NOT NULL, default_value LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, declarations LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX channel_keyname_UNIQUE (channel, keyname), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE package (id INT AUTO_INCREMENT NOT NULL, channel VARCHAR(30) NOT NULL, keyname VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, pname VARCHAR(255) NOT NULL, version VARCHAR(255) NOT NULL, system VARCHAR(255) NOT NULL, available TINYINT(1) DEFAULT NULL, description LONGTEXT DEFAULT NULL, download_page LONGTEXT DEFAULT NULL, homepage LONGTEXT DEFAULT NULL, maintainers LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', outputs_to_install LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', platforms LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', position VARCHAR(255) DEFAULT NULL, license_free TINYINT(1) DEFAULT NULL, license_full_name VARCHAR(255) DEFAULT NULL, license_short_name VARCHAR(255) DEFAULT NULL, license_url LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `option`');
        $this->addSql('DROP TABLE package');
    }
}
