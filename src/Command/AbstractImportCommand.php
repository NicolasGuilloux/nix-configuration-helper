<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Process\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use VDX\Brotli\Exception\BrotliException;

/**
 * Class AbstractImportCommand
 *
 * @package App\Command
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 */
abstract class AbstractImportCommand extends Command
{
    public const BATCH_SIZE = 50;

    protected static string $messageClass;
    protected static string $nixUrlParameter;
    protected ?HttpClientInterface $client = null;
    protected array $errors = [];
    protected MessageBusInterface $messageBus;
    protected ParameterBagInterface $parameterBag;

    /**
     * @required
     *
     * @param MessageBusInterface    $messageBus
     * @param ParameterBagInterface  $parameterBag
     *
     * @return void
     */
    public function initImportCommand(
        MessageBusInterface $messageBus,
        ParameterBagInterface $parameterBag
    ): void {
        $this->messageBus = $messageBus;
        $this->parameterBag = $parameterBag;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $formatter = new SymfonyStyle($input, $output);
        $channels = $this->getChannels();

        foreach ($channels as $channel) {
            $url = sprintf(
                $this->parameterBag->get(static::$nixUrlParameter),
                $channel
            );

            $data = $this->processData($this->getData($url));

            if (empty($data)) {
                continue;
            }

            $formatter->comment('Processing the channel ' . $channel);
            $chunks = array_chunk($data, self::BATCH_SIZE, true);

            foreach($chunks as $chunk) {
                $message = new static::$messageClass($chunk, $channel);
                $this->messageBus->dispatch($message);
            }
        }

        return 0;
    }

    abstract protected function processData(string $data): array;

    /**
     * @return array
     */
    protected function getChannels(): array
    {
        $url = $this->parameterBag->get('nix_channels_url');
        $data = $this->getData($url);

        return json_decode($data, false, 512, JSON_THROW_ON_ERROR);
    }

    protected function getData(string $url): string
    {
        if ($this->client === null) {
            $this->client = HttpClient::create([
                'headers' => [
                    'Accept-Encoding' => 'gzip, br',
                ]
            ]);
        }

        $response = $this->client->request('GET', $url);
        $statusCode = $response->getStatusCode();

        if ($statusCode < 200 || $statusCode > 300) {
            throw new \RuntimeException('Failed to fetch the data');
        }

        return $response->getContent();
    }
}
