<?php

namespace App\Command;

use App\Messenger\Message\OptionsImportMessage;

/**
 * Class AppImportOptions
 *
 * @package App\Command
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 */
class AppImportOptions extends AbstractImportCommand
{
    public static $defaultName = 'app:import:options';

    protected static string $messageClass = OptionsImportMessage::class;
    protected static string $nixUrlParameter = 'nix_options_url';

    public function configure(): void
    {
        $this->setDescription('Fetch the options from the NixOS website and populate the database with it.');
    }

    protected function getChannels(): array
    {
        $channels = parent::getChannels();

        foreach ($channels as $key => $value) {
            if ($value === 'nixpkgs-unstable') {
                $channels[$key] = 'nixos-unstable';
            }
        }

        return $channels;
    }

    protected function processData(string $data): array
    {
        $uncompressed = brotli_uncompress($data);

        return json_decode($uncompressed, true, 512, JSON_THROW_ON_ERROR);
    }
}
