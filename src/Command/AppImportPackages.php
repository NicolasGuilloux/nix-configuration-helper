<?php

namespace App\Command;

use App\Messenger\Message\PackagesImportMessage;

/**
 * Class AppImportPackages
 *
 * @package App\Command
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 */
class AppImportPackages extends AbstractImportCommand
{
    public static $defaultName = 'app:import:packages';

    protected static string $messageClass = PackagesImportMessage::class;
    protected static string $nixUrlParameter = 'nix_packages_url';

    /**
     * @return void
     */
    public function configure(): void
    {
        $this->setDescription('Fetch the options from the NixOS website and populate the database with it.');
    }

    protected function processData(string $data): array
    {
        $uncompressed = brotli_uncompress($data);
        $array = json_decode($uncompressed, true, 512, JSON_THROW_ON_ERROR);

        return $array['packages'];
    }
}
