<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Package
 *
 * @package App\Entity
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 *
 * @ORM\Entity
 */
class Package extends AbstractNixConfig
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30, nullable=false)
     *
     * @Groups({"search_result", "package_channel", "elastica"})
     */
    public $channel;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"search_result", "package_keyname", "elastica"})
     */
    public $keyname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"search_result", "package_name", "elastica"})
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"search_result", "package_pname"})
     */
    public $pname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"search_result", "package_version"})
     */
    public $version;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"search_result", "package_system"})
     */
    public $system;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false, nullable=true)
     *
     * @Groups({"search_result", "package_available"})
     */
    public $available;

    /**
     * @var boolean
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"search_result", "package_description", "elastica"})
     */
    public $description;

    /**
     * @var boolean
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"search_result", "package_download_page"})
     */
    public $downloadPage;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"search_result", "package_homepage"})
     */
    public $homepage;

    /**
     * @var License
     *
     * @ORM\Embedded(class="App\Entity\License", columnPrefix="license_")
     *
     * @Groups({"search_result", "package_license"})
     */
    public $license;

    /**
     * @var boolean
     *
     * @ORM\Column(type="array", nullable=true)
     *
     * @Groups({"search_result", "package_maintainers"})
     * @SWG\Property(type="object")
     */
    public $maintainers;

    /**
     * @var boolean
     *
     * @ORM\Column(type="array", nullable=false)
     *
     * @Groups({"search_result", "package_outputs_to_install"})
     * @SWG\Property(type="array", @SWG\Items(type="string"))
     */
    public $outputsToInstall;

    /**
     * @var boolean
     *
     * @ORM\Column(type="array", nullable=true)
     *
     * @Groups({"search_result", "package_platforms"})
     * @SWG\Property(type="array", @SWG\Items(type="string"))
     */
    public $platforms;

    /**
     * @var boolean
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"search_result", "package_position"})
     */
    public $position;

    /**
     * @param string $channel
     * @param string $keyname
     * @param array  $params
     *
     * @return static
     */
    public static function createFromKeynameAndParameters(string $channel, string $keyname, array $params): self
    {
        $meta = $params['meta'] ?? [];

        $option = new self();
        $option->channel = $channel;
        $option->keyname = $keyname;
        $option->name = $params['name'];
        $option->pname = $params['pname'];
        $option->version = $params['version'];
        $option->system = $params['system'];
        $option->available = $meta['available'] ?? true;
        $option->description = $meta['description'] ?? null;
        $option->downloadPage = $meta['downloadPage'] ?? null;
        $option->homepage = $meta['homepage'] ?? null;
        $option->license = License::createFromParameters($meta['license'] ?? []);
        $option->maintainers = $meta['maintainers'] ?? [];
        $option->outputsToInstall = $meta['outputsToInstall'] ?? [];
        $option->platforms = $meta['platforms'] ?? [];
        $option->position = $meta['position'] ?? null;

        return $option;
    }
}
