<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Option
 *
 * @package App\Entity
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 *
 * @ORM\Entity
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *         name="channel_keyname_UNIQUE",
 *         columns={"channel", "keyname"}
 *     )}
 * )
 */
class Option extends AbstractNixConfig
{
    public const TYPES = [
        'boolean',
        'string',
        'array',
    ];

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30, nullable=false)
     *
     * @Groups({"search_result", "option_channel", "elastica"})
     */
    public $channel;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"search_result", "option_keyname", "elastica"})
     */
    public $keyname;

    /**
     * @ORM\Column(type="text", nullable=false)
     *
     * @Groups({"search_result", "option_type"})
     */
    public $type;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"search_result", "option_read_only"})
     */
    public $readOnly;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"search_result", "option_default_value"})
     */
    public $defaultValue;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"search_result", "option_description", "elastica"})
     */
    public $description;

    /**
     * @ORM\Column(type="array")
     *
     * @Groups({"search_result", "option_declarations"})
     * @SWG\Property(type="array", @SWG\Items(type="string"))
     */
    public $declarations = [];

    /**
     * @param string $channel
     * @param string $keyname
     * @param array  $params
     *
     * @return static
     */
    public static function createFromKeynameAndParameters(string $channel, string $keyname, array $params): self
    {
        $option = new self();
        $option->channel = $channel;
        $option->keyname = $keyname;
        $option->type = $params['type'];
        $option->readOnly = $params['readOnly'];
        $option->defaultValue = json_encode($params['default'] ?? null);
        $option->description = $params['description'];
        $option->declarations = $params['declarations'];

        return $option;
    }
}
