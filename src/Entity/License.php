<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class License
 *
 * @package App\Entity
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 *
 * @ORM\Embeddable()
 */
class License
{
    /**
     * @var string
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups({"search_result", "license_free"})
     */
    public $free;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"search_result", "license_full_name"})
     */
    public $fullName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"search_result", "license_short_name"})
     */
    public $shortName;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"search_result", "license_url"})
     */
    public $url;

    /**
     * @param array|string $data
     *
     * @return static
     */
    public static function createFromParameters($data): self
    {
        $license = new static();

        if (is_string($data)) {
            $license->free = true;
            $license->fullName = $data;
            $license->shortName = $data;

            return $license;
        }

        $license->free = $data['free'] ?? null;
        $license->fullName = $data['fullName'] ?? null;
        $license->shortName = $data['shortName'] ?? null;
        $license->url = $data['url'] ?? null;

        return $license;
    }
}
