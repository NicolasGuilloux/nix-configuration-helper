<?php declare(strict_types=1);

namespace App\Entity;

use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class AbstractNixConfig
 *
 * @package   App\Entity
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
abstract class AbstractNixConfig extends AbstractEntity
{
    /**
     * @internal Used by Nelmio API Doc
     *
     * @var string
     *
     * @Groups({"search_result", "nix_config_type"})
     * @SWG\Property(description="Type of the configuration")
     */
    private $configurationType;

    /**
     * @Groups({"search_result", "nix_config_type"})
     *
     * @return string
     *
     * @throws \ReflectionException
     */
    public function getConfigurationType(): string
    {
        $shortname = (new \ReflectionClass($this))->getShortName();

        return strtolower($shortname);
    }
}
