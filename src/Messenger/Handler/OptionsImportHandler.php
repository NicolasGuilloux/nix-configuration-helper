<?php declare(strict_types=1);

namespace App\Messenger\Handler;

use App\Entity\Option;
use App\Messenger\Message\OptionsImportMessage;

/**
 * Class OptionsImportHandler
 *
 * @package   App\Messenger\Handler
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class OptionsImportHandler extends AbstractImportHandler
{
    public static string $entityClass = Option::class;

    public function __invoke(OptionsImportMessage $message): void
    {
        foreach ($message->options as $keyname => $params) {
            $existingOption = $this->repository->findOneBy([
                'keyname' => $keyname,
                'channel' => $message->channel,
            ]);

            if ($existingOption === null) {
                 $option = Option::createFromKeynameAndParameters(
                     $message->channel,
                     $keyname,
                     $params
                 );

                 $this->entityManager->persist($option);
            }
        }

        $this->entityManager->flush();
    }
}
