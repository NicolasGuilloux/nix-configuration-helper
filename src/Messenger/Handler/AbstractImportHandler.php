<?php declare(strict_types=1);

namespace App\Messenger\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

abstract class AbstractImportHandler implements MessageHandlerInterface
{
    public static string $entityClass;
    protected EntityManagerInterface $entityManager;
    protected ObjectRepository $repository;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(static::$entityClass);
    }
}
