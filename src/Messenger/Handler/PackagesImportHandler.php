<?php declare(strict_types=1);

namespace App\Messenger\Handler;

use App\Entity\Option;
use App\Entity\Package;
use App\Messenger\Message\PackagesImportMessage;

/**
 * Class PackagesImportHandler
 *
 * @package   App\Messenger\Handler
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class PackagesImportHandler extends AbstractImportHandler
{
    public static string $entityClass = Package::class;

    public function __invoke(PackagesImportMessage $message): void
    {
        foreach ($message->packages as $keyname => $params) {
            $existingOption = $this->repository->findOneBy([
                'keyname' => $keyname,
                'channel' => $message->channel,
            ]);

            if ($existingOption === null) {
                $option = Package::createFromKeynameAndParameters(
                    $message->channel,
                    $keyname,
                    $params
                );

                $this->entityManager->persist($option);
            }
        }

        $this->entityManager->flush();
    }
}
