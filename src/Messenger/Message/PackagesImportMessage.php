<?php declare(strict_types=1);

namespace App\Messenger\Message;

use App\Entity\Package;

/**
 * Class PackagesImportMessage
 *
 * @package   App\Messenger\Message
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class PackagesImportMessage
{
    /**
     * @var array|Package[]
     */
    public array $packages;
    public string $channel;

    /**
     * PackagesImportMessage constructor.
     *
     * @param array|Package|Package[] $packages
     * @param string                  $channel
     */
    public function __construct($packages = [], string $channel = '')
    {
        $this->packages = (array) $packages;
        $this->channel = $channel;
    }
}
