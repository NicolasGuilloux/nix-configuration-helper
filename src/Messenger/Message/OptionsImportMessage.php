<?php declare(strict_types=1);

namespace App\Messenger\Message;

use App\Entity\Option;

/**
 * Class OptionsImportMessage
 *
 * @package   App\Messenger\Message
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class OptionsImportMessage
{
    /**
     * @var array|Option[]
     */
    public array $options;
    public string $channel;

    /**
     * OptionsImportMessage constructor.
     *
     * @param array|Option[]|Option $options
     * @param string                $channel
     */
    public function __construct($options = [], string $channel = '')
    {
        $this->options = (array) $options;
        $this->channel = $channel;
    }
}
