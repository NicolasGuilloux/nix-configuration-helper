<?php

namespace App\Repository;

use App\Entity\Option;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class OptionRepository
 *
 * @package App\Repository
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 *
 * @method Option|null findOneByKeyname(string $keyname)
 */
class OptionRepository extends ServiceEntityRepository
{
    /**
     * OptionRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Option::class);
    }

    /**
     * @param string   $channel
     * @param string   $query
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return Paginator
     */
    public function searchByChannel(string $channel, string $query, int $limit = null, int $offset = null): Paginator
    {
        $qb = $this->_em->createQueryBuilder();
        $expr = $qb->expr();

        $qb->select('o')
            ->from(Option::class, 'o')
            ->where($expr->like('o.channel', ':channel'))
            ->andWhere(
                $expr->orX(
                    $expr->like('o.keyname', ':search'),
                    $expr->like('o.description', ':search')
                )
            )
            ->setParameter('channel', $channel)
            ->setParameter('search', '%' . $query . '%');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }

        return new Paginator($qb, true);
    }
}
