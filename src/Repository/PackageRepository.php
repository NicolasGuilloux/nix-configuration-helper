<?php

namespace App\Repository;

use App\Entity\Package;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class PackageRepository
 *
 * @package App\Repository
 * @author  Nicolas Guilloux <novares.x@gmail.com>
 *
 * @method Package|null findOneByKeyname(string $keyname)
 */
class PackageRepository extends ServiceEntityRepository
{
    /**
     * PackageRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Package::class);
    }

    /**
     * @param string   $channel
     * @param string   $query
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return Paginator
     */
    public function searchByChannel(string $channel, string $query, int $limit = null, int $offset = null): Paginator
    {
        $qb = $this->_em->createQueryBuilder();
        $expr = $qb->expr();

        $qb->select('p')
            ->from(Package::class, 'p')
            ->where($expr->like('p.channel', ':channel'))
            ->andWhere(
                $expr->orX(
                    $expr->like('p.keyname', ':search'),
                    $expr->like('p.name', ':search')
                )
            )
            ->setParameter('channel', $channel)
            ->setParameter('search', '%' . $query . '%');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }

        return new Paginator($qb, true);
    }
}
